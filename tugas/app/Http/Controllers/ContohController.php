<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\mahasiswa;

class ContohController extends Controller
{
    public function tampilkan($nama)
    {
    	return view('content')->with('var_nama', $nama);
    }

    public function input()
    {
    	return view('input_form');
    }

    public function input_post(Request $request)
    {
    	$nama = $request->nama;
    	$alamat = $request->alamat;
    	$no_telp = $request->no_telp;
    	return view('view_form')
    	->with('nama', $nama)
    	->with('alamat', $alamat)
    	->with('no_telp', $no_telp);
    }

    public function tabel()
    {
    	// $mahasiswa = mahasiswa::all();
    	$mahasiswa = mahasiswa::where('id','>=','2')->get();
    	return view('tabel')->with('mahasiswa', $mahasiswa);
    }
}
