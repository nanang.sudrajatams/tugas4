@extends('layouts.layout')

@section('title')
	content 1
@endsection

@section('content')
	<form action="input_form_post" method="POST">
		{{csrf_field()}}
		<table>
			<tr>
				<td>Nama</td>
				<td><input type="text" name="nama"></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><input type="text" name="alamat"></td>
			</tr>
			<tr>
				<td>No Telp</td>
				<td><input type="text" name="no_telp"></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="submit" value="Save"></td>
			</tr>
		</table>
	</form>
@endsection