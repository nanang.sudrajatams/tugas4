<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome_laravel');
});

Route::get('/profile', function () {
    return('ini adalah page profile');
});

Route::get('/contact', function () {
    return view('template.contact');
});

// Route::get('/content', function () {
//     return view('content');
// });

Route::get('/content_lain', function () {
    return view('content_lain');
});

// Route::get('/profile_view', function () {
//     return view('page_profile');
// });

Route::prefix('admin')->group(function () {
    Route::get('users', function () {
        return('ini page Admin / Users');
    });

    Route::get('users1', function () {
        return('ini page Admin1 / Users1');
    });
});

Route::get('/profile_view/{id}/{kelas}', function ($nama, $kelas) {
    return view('page_profile')
    ->with('var_nama', $nama)
    ->with('var_kelas', $kelas);
});

Route::get('/content/{id}', 'ContohController@tampilkan');
Route::get('/input_form', 'ContohController@input');
Route::post('/input_form_post', 'ContohController@input_post');
Route::get('/tabel', 'ContohController@tabel');
